#Dataharvest 2019 - Investigating with Python and OSINT 1/2

It is impossible to teach Python in 2 sessions. Nonetheless, you don't need to be a god at coding to be able to build little tools, using Python and... a little bit of Google and Stackoverflow.
The purpose of this workshop is to show you that everybody can do a little bit of Python and this language can save you lots of time.

__Don't show off. Trying to re-invent the wheel is kind of dumb.__

Keypoints : python, google dorks.

Pre-requites : Python3 on the machine. A browser. A text editor.
If no Python, try this : https://jupyter.org/try

##1- What's the dataset & what do I want to do?

- More than 100 large donors of Emmanuel Macron campaign, found by Le Monde
- The French gazette, where all the honors and distinctions ("légion d'honneur") given by the French government are published

We'd like to know if Emmanuel Macron has given this medal to some of his biggest donors. We could of course check them one by one "by hand", but it would take (too much) time.

What would you do ? 

##2 - How do I automate that?

###1°step - install Python librairy

Google package has one dependency on beautifulsoup.

	pip install beautifulsoup4

Then install google package

	pip install google

###2° step : reading documentation
    search(query, tld='com', lang='en', num=10, start=0, stop=None, pause=2.0)

- query : query string that we want to search for.
- tld : tld stands for top level domain which means we want to search our result on google.com or google.in or some other domain.
- lang : lang stands for language.
- num : Number of results we want.
- start : First result to retrieve.
- stop : Last result to retrieve. Use None to keep searching forever.
- pause : Lapse to wait between HTTP requests. Lapse too short may cause Google to block your IP. Keeping significant lapse will make your program slow but its safe and better option.

###4° Step : Do your own program


    from googlesearch import search
    query = "'NAME (SURNAME' promotion et nomination légion d'honneur site:https://www.legifrance.gouv.fr"
    for j in search(query, tld="fr", num=10, stop=1, pause=2):
        print(j)

New concepts : load library, for loop, indentation
Let's iterate this.

    from googlesearch import search
    list_to_test=[
    'NAME (SURNAME',
    'NAME (SURNAME',
    'NAME (SURNAME']
    for i in list_to_test:
        query="\""+i+"\""+" "+"promotion et nomination légion honneur site:https://www.legifrance.gouv.fr after:2017/05/07"
        #print(i+"| ")

        #print the result
        for j in search(query, tld="fr", num=10, stop=1, pause=5):
            print(i+"| "+j)


Vocabulary : print, list, requests/rate limit







